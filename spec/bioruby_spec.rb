# -*- coding: utf-8 -*-

# http://thebird.nl/bioruby/Tutorial.rd.html

require "bio"

describe "BioRuby" do
  seq = Bio::Sequence::NA.new("atgcatgcaaaa")

  it "complementを得る" do 
    expect(seq.complement).to eq("ttttgcatgcat")
  end
  it "translateでアミノ酸に翻訳する" do
    expect(seq.translate).to eq("MHAK")
  end

  describe Bio::CodonTable do
    
  end
end
