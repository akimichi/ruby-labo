# describe Pathname do
#   require 'pathname'
  
#   before do
#     @path = Pathname('spec/file_spec.rb')
#     @root_path = Pathname('spec/file_spec')
#     @dir_path = Pathname('spec')
#   end

#   it "should to_s" do
#     expect(@path.to_s).to eq('spec/file_spec.rb')
#   end

#   it "should get basename" do
#     expect(@path.basename.to_s).to eq('file_spec.rb')
#     expect(@root_path.basename.to_s).to eq('file_spec')
#   end

#   it "should get dirname" do
#     expect(@path.dirname.to_s).to eq('spec')
#     expect(@root_path.dirname.to_s).to eq('spec')
#   end

#   it "should check directory" do
#     expect(@dir_path.directory?).to eq(true)
#   end

#   it "should check existence of the file" do
#     expect(@path.exist?).to eq(true)
#   end

#   it "should create file path" do
#     expect(@dir_path + 'file_spec.rb').to eq(Pathname('spec/file_spec.rb'))
#   end

# end

