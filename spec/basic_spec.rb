# -*- coding: utf-8 -*-
describe Integer do
  before do
    @zero = 0
    @one = 0
  end

  it "is_a? で Integer かどうかを判定する" do
    expect(@zero.is_a?(Integer)).to eq(true)
  end
end

describe String do
  before do
    @str = "string"
  end

  it "should be mutable" do
    str = "string"
    expect(str.upcase!).to eq("STRING")
    expect(str).to eq("STRING")
    # str = "hello"
    # str << " world"
    # expect(str).to eq("hello world")
    #@empty_array.should be_empty
  end
end

