# -*- coding: utf-8 -*-

require "bio"

describe "バイオメディカル解析実習" do
  describe "第2回講義の課題" do
    # DNA配列AをRNA配列に変換する
    # DNA配列Aにコードされる一番長いアミノ酸を示す。
    # DNA配列A : 5'-TGATATGCAATGGCAGGAATTAGCCATATATAACTAGATCA-3'
    dna_seq = "TGATATGCAATGGCAGGAATTAGCCATATATAACTAGATCA"
    it "DNA配列をRNAに変換する" do
      expect(dna_seq.tr("T","U")).to eq("UGAUAUGCAAUGGCAGGAAUUAGCCAUAUAUAACUAGAUCA")
    end

    it "DNA配列をアミノ酸に変換する" do
      seq = Bio::Sequence::NA.new(dna_seq)
      expect(seq.translate).to eq("*YAMAGISHI*LD")
    end

    it "reading frame を考慮して開始コドンから停止コドンまでのDNA配列をアミノ酸に変換し、そのなかで最長のアミノ酸を求める" do
      seq = Bio::Sequence::NA.new("TGATATGCAATGGCAGGAATTAGCCATATATAACTAGATCA")
      answer = [1,2,3,4,5,6].map {|frame| 
        seq.translate(frame)
      }.collect {|amino_seq|
        amino_seq.scan(/(M[^*]+)\*/)
      }.flatten.max
      expect(answer).to eq("MQWQELAIYN")
    end
    it "開始コドンが複数出現する配列において、reading frame を考慮して開始コドンから停止コドンまでのDNA配列をアミノ酸に変換し、そのなかで最長のアミノ酸を求める" do
      seq = Bio::Sequence::NA.new("CACATGGCATAAAATGATATGCAATGGCAGGAATTAGCCATATATAACTAGATCA")
      answer = (1..seq.length).map {|frame| 
        seq.translate(frame)
      }.collect {|amino_seq|
        amino_seq.scan(/(M[^*]+)\*/)
      }.flatten.max
      expect(answer).to eq("MQWQELAIYN")
    end
  end
  describe "第4回講義の課題" do
    
    
  end
  describe "5回講義の課題" do
    before do
      @refGene = "spec/refGene.txt"
      @file = File.open(@refGene)
    end

    it "should open" do
      lines = @file.readlines
      lines.each {|line|
        chr = line.split("\t")[2]
        # p line if chr == "chr1"
      }
    end

    it "課題) 転写物の長さの平均と分散をストランドごとに計算し、統計的に検討する" do
      class Array 
        def avg
          inject(0.0){|r,i| 
            r+=i.to_i
          }/size
        end
        # 要素をto_iした値の分散を算出する
        def variance
          a = avg
          inject(0.0){|r,i|
            r+=(i.to_i - a)**2
          }/size
        end
        # 要素をto_iした値の標準偏差を算出する
        def standard_deviation
          Math.sqrt(variance)
        end
      end

      lines = @file.readlines
      lengths = []
      lines.each {|line|
        last = line.split("\t")[5].to_i
        start = line.split("\t")[4].to_i
        lengths << last - start
      }
      expect(lengths.avg).to eq(55991.06519123419)
      expect(lengths.standard_deviation).to eq(112633.7727829615)
    end

    after do
      @file.close
    end
  end
  describe "6回講義の課題" do
    # 0         1               2       3       4       5       6       7       8       9                       10                      11      12      13      14      15
    # 637	NM_021010	chr8	-	6912828	6914259	6912952	6914219	2	6912828,6914047,	6913065,6914259,	0	DEFA5	cmpl	cmpl	1,0,

    #[0] `bin` smallint(5) unsigned NOT NULL,
    #[1] `name` varchar(255) NOT NULL,
    #[2] `chrom` varchar(255) NOT NULL,
    #[3] `strand` char(1) NOT NULL,
    #[4] `txStart` int(10) unsigned NOT NULL,
    #[5] `txEnd` int(10) unsigned NOT NULL,
    #[6] `cdsStart` int(10) unsigned NOT NULL,
    #[7] `cdsEnd` int(10) unsigned NOT NULL,
    #[8] `exonCount` int(10) unsigned NOT NULL,
    #[9] `exonStarts` longblob NOT NULL,
    #[10] `exonEnds` longblob NOT NULL,
    #[11] `score` int(11) DEFAULT NULL,
    #[12] `name2` varchar(255) NOT NULL,
    # `cdsStartStat` enum('none','unk','incmpl','cmpl') NOT NULL,
    # `cdsEndStat` enum('none','unk','incmpl','cmpl') NOT NULL,
    # `exonFrames` longblob NOT NULL,

    before do
      @refGene = "spec/refGene.txt"
      @file = File.open(@refGene)
    end

    it "ex5 平均、標準偏差を計算する" do
      class Array 
        def avg
          inject(0.0){|r,i| 
            r+=i.to_i
          }/size
        end
        # 要素をto_iした値の分散を算出する
        def variance
          a = avg
          inject(0.0){|r,i|
            r+=(i.to_i - a)**2
          }/size
        end
        # 要素をto_iした値の標準偏差を算出する
        def standard_deviation
          Math.sqrt(variance)
        end
      end

      lines = @file.readlines
      lengths = []
      lines.each {|line|
        last = line.split("\t")[5].to_i
        start = line.split("\t")[4].to_i
        lengths << last - start
      }
      expect(lengths.avg).to eq(55991.06519123419)
      expect(lengths.standard_deviation).to eq(112633.7727829615)
    end

    it "ex7" do
      
    end
    after do
      @file.close
    end
  end
  describe "7回講義の課題" do
    # ヒトの last エクソンのうち短いもの(<100)を取り出し、どのような機能の遺伝子が有意に見られるか調べよ
    # 1. 該当する遺伝子を1つあげ、その遺伝子の
    #  * 最終エクソンの長さ
    #  * 遺伝子の機能
    #  * 染色体番号と座標
    #  * 遺伝子をコードするRNAの長さとペプチド配列の長さ
    #  について記載せよ。
    # 2. 短い lastエクソンを持つ遺伝子群全体に関してどのようなことが言えるか DAVID で見る
    before do
      @refGene = "spec/refGene.txt"
      @file = File.open(@refGene)

      lines = @file.readlines
      lengths = []
      lines.each {|line|
        name = line.split("\t")[1]
        chr = line.split("\t")[2]
        strand = line.split("\t")[3]
        starts = line.split("\t")[4].split(",").to_i
        lasts = line.split("\t")[5].split(",").to_i
        exon_count = line.split("\t")[8].to_i
        
        starts[exon_count-1] - 
        exon_len = last - start
      }
    end

    after do
      @file.close
    end
  end
end

