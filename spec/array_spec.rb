describe Array, "when empty" do
  before do
    @empty_array = []
  end

  it "should be empty" do
    expect(@empty_array).to eq([])
    #@empty_array.should be_empty
  end

  it "should size 0" do
    expect(@empty_array.size).to eq(0)
    #@empty_array.size.should == 0
  end
  
  after do
    @empty_array = nil
  end
end

describe Array, "when not empty" do
  before do
    @array = [0,1,2,3]
  end

  it "should shift the first element" do
    expect(@array.shift).to eq(0)
    expect(@array).to eq([1,2,3])
  end
end
